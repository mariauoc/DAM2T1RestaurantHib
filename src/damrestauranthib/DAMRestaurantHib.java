/*
 * Ejemplo de DAM2T1 con Hibernate
 */
package damrestauranthib;

import entities.Cocinero;
import entities.Plato;
import exceptions.RestaurantException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import persistence.RestaurantDAO;

/**
 *
 * @author mfontana
 */
public class DAMRestaurantHib {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Empezando Test con Hibernate");
        System.out.println("Estableciendo conexión");
        RestaurantDAO restaurantDAO = new RestaurantDAO();
        System.out.println("Conexión establecida");

        Cocinero c = new Cocinero("dam", "1", "m", 0, 0, "es");
        System.out.println("Insertando cocinero...");
        try {
            restaurantDAO.insertarCocinero(c);
            System.out.println("Cocinero insertado");

        } catch (RestaurantException ex) {
            System.out.println(ex.getMessage());
        }

        try {
            // Modificar la especialidad de un objeto
            restaurantDAO.modificarEspecialidadCocinero(c, "Macarrones");
            System.out.println("Especialidad modificada");
        } catch (RestaurantException ex) {
            System.out.println(ex.getMessage());
        }
        
        System.out.println("Borrando cocinero...");
        try {
            restaurantDAO.borrarCocinero(c);
            System.out.println("Cocinero borrado");

        } catch (RestaurantException ex) {
            System.out.println(ex.getMessage());
        }
        
        String nombre = "Susana Raton";
        System.out.println("Ver los platos de un cocinero : "+nombre);
        Cocinero susana = restaurantDAO.getCocineroByNombre(nombre);
        Set<Plato> platos = susana.getPlatos();
        for (Plato p : platos) {
            System.out.println(p);
        }

        
        System.out.println("Listado de todos los cocineros");
        List<Cocinero> cocineros = restaurantDAO.selectAllCocinero();
        for (Cocinero a : cocineros) {
            System.out.println(a);
        }
        
        System.out.println("Cerrando sesión");
        restaurantDAO.cerrar();
        System.out.println("Conexión cerrada.");
    }

}
