/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import entities.Cocinero;
import exceptions.RestaurantException;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author mfontana
 */
public class RestaurantDAO {

    private Session sesion;
    private Transaction tx;

    public void insertarCocinero(Cocinero c) throws RestaurantException  {
        if (existeCocinero(c)) {
            throw new RestaurantException("Ya existe un cocinero con ese nombre");
        }
        tx = sesion.beginTransaction();
        sesion.save(c);
        tx.commit();
    }
    
    public void modificarEspecialidadCocinero(Cocinero c, String nuevaEspecialidad) throws RestaurantException {
        Cocinero aux = getCocineroByNombre(c.getNombre());
        if (aux != null) {
           aux.setEspecialidad(nuevaEspecialidad);
           tx = sesion.beginTransaction();
           sesion.update(aux);
           tx.commit();
        } else {
            throw new RestaurantException("No existe cocinero");
        }
    }

    public void borrarCocinero(Cocinero c) throws RestaurantException  {
        Cocinero aux = (Cocinero) sesion.get(Cocinero.class, c.getNombre());
        if (aux != null) {
            tx = sesion.beginTransaction();
            sesion.delete(aux);
            tx.commit();
        } else {
            throw new RestaurantException("No existe el cocinero");
        }
    }

    public List<Cocinero> selectAllCocinero() {
        Query q = sesion.createQuery("select c from Cocinero c");
        return q.list();
    }
    public boolean existeCocinero(Cocinero c) {
        return getCocineroByNombre(c.getNombre()) != null;
    }

    public Cocinero getCocineroByNombre(String nombre) {
        return (Cocinero) sesion.get(Cocinero.class, nombre);
    }

    public RestaurantDAO() {
        sesion = HibernateUtil.getSessionFactory().openSession();
    }

    public void cerrar() {
        sesion.close();
        HibernateUtil.close();
    }

}
